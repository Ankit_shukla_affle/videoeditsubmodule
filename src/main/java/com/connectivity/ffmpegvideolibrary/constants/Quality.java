package com.connectivity.ffmpegvideolibrary.constants;

/**
 * Developer: ankit
 * Dated: 11/10/17.
 */

public interface Quality {

    String LOW = "LOW";
    String MEDIUM = "MEDIUM";
    String HIGH = "HIGH";
}
